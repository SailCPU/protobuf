git submodule update --init --recursive
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=~/workspace/oss -Dprotobuf_BUILD_LIBPROTOC=TRUE ..
make install -j4
